## Search-Term bash script.

Ce script vous permet de rechercher si un terme existe dans un fichier et si oui de rediriger toutes les lignes correspondantes dans un fichier texte.

1. Télécharger le script : 
`wget https://gitlab.com/Natizyskunk/search-term/raw/master/search-term.sh`

2. Rendre le script executable :
`$ chmod +x search-term.sh`

3. Lancer le script.
`$ ./search-term.sh`

La vous seras demandé d'entrer le nom du fichier dans lequel chercher (il est également possible de mettre un chemin) puis le terme à rechercher.
Si le terme est trouvé, le script affiche "SUCCESS" et redirige toutes les occurrences ainsi que leur numéro de ligne dans un fichier nommé "mygrep.txt" situé dans le dossier courant.
Si le terme est introuvable, le script affiche le message "NOT FOUND".
