#!/bin/bash
read -p "Enter the file name where you want to search : "  FILE
myfile=$FILE
read -p "Enter the term to search in the file : "  TERM
myterm=$TERM

if grep -Rq "$myterm" $myfile
then
	echo "SUCCESS"
	grep -Rn --color "$myterm" $myfile > mygrep.txt
else
  echo 'NOT FOUND !'
fi